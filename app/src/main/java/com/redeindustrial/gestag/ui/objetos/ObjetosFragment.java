package com.redeindustrial.gestag.ui.objetos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.redeindustrial.gestag.ObjetoActivity;
import com.redeindustrial.gestag.R;
import com.redeindustrial.gestag.databinding.FragmentObjetosBinding;
import com.redeindustrial.gestag.entidades.Config;
import com.redeindustrial.gestag.entidades.Objeto;
import com.redeindustrial.gestag.entidades.Usuario;
import com.redeindustrial.gestag.utils.Conexao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ObjetosFragment extends Fragment {

    private ObjetosViewModel objetosViewModel;
    private FragmentObjetosBinding binding;

    private final ArrayList<Objeto> objetos = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        objetosViewModel = new ViewModelProvider(this).get(ObjetosViewModel.class);

        binding = FragmentObjetosBinding.inflate(inflater, container, false);

        Usuario usuario = Config.getInstance(getContext()).usuario;

        binding.toolbar.setTitle("Olá, "+usuario.getNome());

        MAdapter adapter = new MAdapter();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);

        new Conexao(binding.progress){

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void retorno(JSONObject json) throws JSONException {

                if(!json.getBoolean("success")) {
                    Toast.makeText(getContext(), json.getString("message"), Toast.LENGTH_SHORT).show();
                    binding.painelVazio.setVisibility(View.VISIBLE);
                    return;
                }

                JSONArray data = json.getJSONArray("data");

                for(int i=0; i<data.length(); i++) {
                    objetos.add(new Objeto(data.getJSONObject(i)));
                }

                adapter.notifyDataSetChanged();

            }

            @Override
            public void erro(String erro) {
                Log.i("teste", "ue??");
                Toast.makeText(getContext(), erro, Toast.LENGTH_SHORT).show();
            }

        }.executeAsync("http://dev.gestag.com.br/api/v1/objetos", null, Conexao.Tipo.GET, usuario.getToken());

        /*
        objetosViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
        */
        return binding.getRoot();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    class MAdapter extends RecyclerView.Adapter<MAdapter.ObjetoHolder> {

        @NonNull
        @Override
        public ObjetoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ObjetoHolder(getLayoutInflater().inflate(R.layout.item_objeto, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ObjetoHolder holder, int position) {
            holder.set(objetos.get(position));
        }

        @Override
        public int getItemCount() {
            return objetos.size();
        }

        class ObjetoHolder extends RecyclerView.ViewHolder {

            TextView codigo, descricao, mac, porta, status;

            public ObjetoHolder(@NonNull View itemView) {
                super(itemView);

                codigo = itemView.findViewById(R.id.item_objeto_codigo);
                descricao = itemView.findViewById(R.id.item_objeto_descricao);
                mac = itemView.findViewById(R.id.item_objeto_mac);
                porta = itemView.findViewById(R.id.item_objeto_porta);
                status = itemView.findViewById(R.id.item_objeto_status);

            }

            public void set(Objeto objeto) {

                codigo.setText(objeto.getCodigo());
                descricao.setText(objeto.getDescricao());
                mac.setText(objeto.getMac());
                porta.setText("Porta: "+objeto.getPorta());

                if(objeto.isStatus()) {
                    status.setText(R.string.online);
                    status.setTextColor(ResourcesCompat.getColor(getResources(), R.color.verde, null));
                } else {
                    status.setText(R.string.offline);
                    status.setTextColor(ResourcesCompat.getColor(getResources(), R.color.vermelho, null));
                }

                itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(itemView.getContext(), ObjetoActivity.class);
                    intent.putExtra(Objeto.EXTRA, objeto);
                    startActivity(intent);
                });

            }

        }

    }

}