package com.redeindustrial.gestag.ui.objetos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.redeindustrial.gestag.entidades.Objeto;

import java.util.ArrayList;

public class ObjetosViewModel extends ViewModel {

    private MutableLiveData<ArrayList<Objeto>> objetos;

    public ObjetosViewModel() {
        objetos = new MutableLiveData<>();
        objetos.setValue(new ArrayList<>());
    }

    public LiveData<ArrayList<Objeto>> getObjetos() {
        return objetos;
    }

}