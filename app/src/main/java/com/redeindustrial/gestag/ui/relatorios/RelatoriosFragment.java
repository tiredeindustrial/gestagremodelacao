package com.redeindustrial.gestag.ui.relatorios;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.redeindustrial.gestag.R;
import com.redeindustrial.gestag.databinding.FragmentRelatoriosBinding;

public class RelatoriosFragment extends Fragment {

    private RelatoriosViewModel relatoriosViewModel;
    private FragmentRelatoriosBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        relatoriosViewModel =
                new ViewModelProvider(this).get(RelatoriosViewModel.class);

        binding = FragmentRelatoriosBinding.inflate(inflater, container, false);
        binding.toolbar.setTitle(getString(R.string.title_dashboard));

        /*
        final TextView textView = binding.textDashboard;
        relatoriosViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        */
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}