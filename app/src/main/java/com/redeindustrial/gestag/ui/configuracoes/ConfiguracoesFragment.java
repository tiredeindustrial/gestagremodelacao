package com.redeindustrial.gestag.ui.configuracoes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.redeindustrial.gestag.CadastroActivity;
import com.redeindustrial.gestag.LoginActivity;
import com.redeindustrial.gestag.R;
import com.redeindustrial.gestag.databinding.FragmentConfiguracoesBinding;
import com.redeindustrial.gestag.entidades.Config;
import com.redeindustrial.gestag.entidades.Usuario;
import com.redeindustrial.gestag.utils.Persistencia;
import com.redeindustrial.gestag.utils.Utils;

public class ConfiguracoesFragment extends Fragment implements View.OnClickListener {

    private Context context;

    private ConfiguracoesViewModel configuracoesViewModel;
    private FragmentConfiguracoesBinding binding;

    private Config config;

    private String[] itemsModoNoturno;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        configuracoesViewModel = new ViewModelProvider(this).get(ConfiguracoesViewModel.class);

        binding = FragmentConfiguracoesBinding.inflate(inflater, container, false);

        context = getContext();
        itemsModoNoturno = getResources().getStringArray(R.array.modo_noturno);

        config = Config.getInstance(context);

        binding.toolbar.setTitle(getString(R.string.title_notifications));

        binding.btEditarCadastro.setOnClickListener(this);
        binding.btModoNoturno.setOnClickListener(this);
        binding.btLogout.setOnClickListener(this);
        binding.btMaisApps.setOnClickListener(this);
        binding.btAvalieNos.setOnClickListener(this);
        binding.btSobre.setOnClickListener(this);

        binding.etEmail.setText(config.usuario.getEmail());
        binding.modoNoturnoValor.setText(itemsModoNoturno[config.modo_noturno]);

        /*
        final TextView textView = binding.textNotifications;
        configuracoesViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return binding.getRoot();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void editarCadastro() {
        startActivity(new Intent(context, CadastroActivity.class));
    }

    private void modoNoturnoDialog() {

        final int[] position = {config.modo_noturno};

        new AlertDialog.Builder(context)
                .setSingleChoiceItems(itemsModoNoturno, config.modo_noturno, (dialog, which) -> position[0] = which)
                .setPositiveButton(R.string.confirmar, (dialog, which) -> {

                    config.modo_noturno = position[0];
                    if(Persistencia.salvar(context, config, Config.EXTRA)) {

                        switch (position[0]) {
                            case Config.MODO_NOTURNO_ATIVADO:
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                                break;
                            case Config.MODO_NOTURNO_DESATIVADO:
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                                break;
                            default:
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                                break;
                        }

                        binding.modoNoturnoValor.setText(itemsModoNoturno[config.modo_noturno]);

                    }

                })
                .setNegativeButton(R.string.cancelar, null)
                .show();

    }

    private void logout() {

        new AlertDialog.Builder(context)
                .setMessage(R.string.logout_mensagem)
                .setPositiveButton(R.string.sim, (dialog, which) -> {
                    config.usuario = null;
                    if(Persistencia.salvar(context, config, Config.EXTRA)) {
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.putExtra(Config.EXTRA, config);
                        startActivity(intent);
                        ((Activity) context).finish();
                    }
                })
                .setNegativeButton(R.string.cancelar, null)
                .show();

    }

    private void maisApps() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/developer?id=Rede+Industrial"));
        startActivity(intent);
    }

    private void avalieNos() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id="+context.getPackageName()));
        startActivity(intent);
        Log.i("teste", "package: "+context.getPackageName());
    }

    private void infoSobre() {

        View.OnClickListener clickSobre = v -> {

            Intent intent = new Intent();

            switch (v.getId()) {

                case R.id.sobre_telefone:
                    intent.setAction(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+getString(R.string.telefone_numero)));
                    break;

                case R.id.sobre_whatsapp:
                    intent.setAction(Intent.ACTION_VIEW);
                    try {
                        context.getPackageManager().getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                        intent.setData(Uri.parse("https://api.whatsapp.com/send?phone="+getString(R.string.whatsapp_numero)));
                        intent.setPackage("com.whatsapp");
                    } catch (PackageManager.NameNotFoundException e) {
                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp"));
                        Toast.makeText(context, "O Whatsapp não está instalado nesse dispositivo", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case R.id.sobre_email:
                    intent.setAction(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:"+getString(R.string.email_sac)));
                    break;

                case R.id.sobre_site:
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://"+getString(R.string.endereco_site_gestag)));
                    break;

            }

            startActivity(intent);

        };

        View sobreView = getLayoutInflater().inflate(R.layout.layout_sobre, null);

        sobreView.findViewById(R.id.sobre_telefone).setOnClickListener(clickSobre);
        sobreView.findViewById(R.id.sobre_whatsapp).setOnClickListener(clickSobre);
        sobreView.findViewById(R.id.sobre_email).setOnClickListener(clickSobre);
        sobreView.findViewById(R.id.sobre_site).setOnClickListener(clickSobre);

        ((TextView) sobreView.findViewById(R.id.et_telefone))
                .setText(Utils.addMask(getString(R.string.telefone_numero), "(##) ####-####"));

        ((TextView) sobreView.findViewById(R.id.tv_whatsapp))
                .setText(Utils.addMask(getString(R.string.whatsapp_numero).substring(2), "(##) #####-####"));

        new AlertDialog.Builder(context)
                .setView(sobreView)
                .setPositiveButton(R.string.fechar, null)
                .show();

    }

    @Override
    public void onClick(View v) {

        if(v == binding.btEditarCadastro) {
            editarCadastro();
        } else if(v == binding.btModoNoturno) {
            modoNoturnoDialog();
        } else if(v == binding.btLogout) {
            logout();
        } else if(v == binding.btMaisApps) {
            maisApps();
        } else if(v == binding.btAvalieNos) {
            avalieNos();
        } else if(v == binding.btSobre) {
            infoSobre();
        }

    }

}