package com.redeindustrial.gestag.ui.relatorios;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RelatoriosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public RelatoriosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Esta será a tela de Relatórios");
    }

    public LiveData<String> getText() {
        return mText;
    }
}