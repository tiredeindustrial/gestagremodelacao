package com.redeindustrial.gestag;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.redeindustrial.gestag.entidades.Config;
import com.redeindustrial.gestag.entidades.Usuario;
import com.redeindustrial.gestag.utils.Conexao;
import com.redeindustrial.gestag.utils.Persistencia;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class CadastroActivity extends AppCompatActivity {

    private TextInputEditText et_nome;
    private TextInputEditText et_email;
    private MaskedEditText et_telefone;
    private TextInputEditText et_senha_atual;
    private TextInputEditText et_senha_nova;
    private TextInputEditText et_senha_repete;
    private View progress;

    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        init_componentes();

        config = Config.getInstance(this);

        if(config.usuario == null) {
            findViewById(R.id.cadastro_senha_atual_layout).setVisibility(View.GONE);
        } else {
            et_nome.setText(config.usuario.getNome());
            et_email.setText(config.usuario.getEmail());
            et_telefone.setText(config.usuario.getTelefone());
            ((TextView) findViewById(R.id.cadastro_senha_titulo)).setText("Alterar a senha");
            ((TextView) findViewById(R.id.bt_submit)).setText("Editar");
        }

    }

    private void init_componentes() {
        et_nome = findViewById(R.id.et_nome);
        et_email = findViewById(R.id.et_email);
        et_telefone = findViewById(R.id.et_telefone);
        et_senha_atual = findViewById(R.id.et_senha_atual);
        et_senha_nova = findViewById(R.id.et_senha_nova);
        et_senha_repete = findViewById(R.id.et_senha_repete);
        progress = findViewById(R.id.progress);
    }

    public void cancelar(View view) {
        finish();
    }

    public void cadastrar(View view) {

        Map<String, String> formulario = validacao();

        if(formulario != null) {

            if(config.usuario == null) {

                new Conexao(progress){
                    @Override
                    public void retorno(JSONObject json) throws JSONException {

                        if(!json.getBoolean("success")) {
                            Toast.makeText(CadastroActivity.this, json.getString("error"), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        JSONObject data = json.getJSONObject("data");

                        config.usuario = new Usuario(
                                data.getString("USU_CODIGO"),
                                formulario.get("nome"),
                                formulario.get("email"),
                                formulario.get("telefone"),
                                data.getString("PERMISSAO"),
                                LoginActivity.genAuthKey(formulario.get("email"), formulario.get("senha"))
                        );

                        if(Persistencia.salvar(CadastroActivity.this, config, Config.EXTRA)) {

                            Intent intent = new Intent(CadastroActivity.this, MainActivity.class);
                            intent.putExtra(Config.EXTRA, config);
                            startActivity(intent);
                            finishAffinity();

                            Toast.makeText(CadastroActivity.this, "Usuário cadastrado com sucesso", Toast.LENGTH_SHORT).show();

                        }
                        Log.i("teste", json.toString());

                    }

                    @Override
                    public void erro(String erro) {
                        Toast.makeText(CadastroActivity.this, erro, Toast.LENGTH_SHORT).show();
                        Log.e("teste", erro);
                    }

                }.executeAsync("http://dev.gestag.com.br/api/v1/cadastrar", formulario, Conexao.Tipo.POST, null);

            } else {

                new Conexao(progress){
                    @Override
                    public void retorno(JSONObject json) throws JSONException {

                        if(!json.getBoolean("success")) {
                            Toast.makeText(CadastroActivity.this, json.getString("error_description"), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        config.usuario.setNome(formulario.get("nome"));
                        config.usuario.setEmail(formulario.get("email"));
                        config.usuario.setTelefone(formulario.get("telefone"));

                        if(!et_senha_atual.getText().toString().isEmpty()) {
                            config.usuario.setToken(LoginActivity.genAuthKey(formulario.get("email"), formulario.get("senha")));
                        }

                        if(Persistencia.salvar(CadastroActivity.this, config, Config.EXTRA)) {
                            Toast.makeText(CadastroActivity.this, "Usuário editado com sucesso", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK, getIntent().putExtra(Config.EXTRA, config));
                            finish();
                        }

                    }

                    @Override
                    public void erro(String erro) {
                        Log.e("teste", erro);
                    }
                }.executeAsync("http://dev.gestag.com.br/api/v1/editar", formulario, Conexao.Tipo.POST, config.usuario.getToken());

            }

        }

    }

    private Map<String, String> validacao() {

        String nome = et_nome.getText().toString().trim();
        String email = et_email.getText().toString().trim();
        String telefone = et_telefone.getRawText();
        String senha_atual = et_senha_atual.getText().toString();
        String senha_nova = et_senha_nova.getText().toString();
        String senha_repete = et_senha_repete.getText().toString();

        boolean valido = true;

        if(nome.isEmpty()) {
            et_nome.setError("Campo de nome não pode ficar vazio");
            valido = false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("E-mail inválido");
            valido = false;
        }

        if(telefone.length() != 11) {
            et_telefone.setError("Telefone inválido");
            valido = false;
        }

        if(config.usuario == null) {

            if(senha_nova.isEmpty()) {
                et_senha_nova.setError("Digite uma senha");
                valido = false;
            } else if(!senha_nova.equals(senha_repete)) {
                et_senha_nova.setError("As senhas não conferem");
                et_senha_repete.setError("As senhas não conferem");
                valido = false;
            }

        } else {

            if(!senha_nova.isEmpty()) {

                if(senha_atual.isEmpty()) {
                    et_senha_atual.setError("Para alterar a senha, digite a senha atual");
                    valido = false;
                }

                if(!senha_nova.equals(senha_repete)) {
                    et_senha_nova.setError("As senhas não conferem");
                    et_senha_repete.setError("As senhas não conferem");
                    valido = false;
                }

            }

        }

        if(valido) {

            Map<String, String> map = new HashMap<>();
            map.put("nome", nome);
            map.put("email", email);
            map.put("telefone", telefone);
            map.put("senha", senha_nova);

            if(config.usuario != null) {
                map.put("codigo", config.usuario.getCodigo());
                map.put("senha_atual", senha_atual);
            }

            return map;

        }

        return null;

    }

}