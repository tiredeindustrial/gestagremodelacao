package com.redeindustrial.gestag;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.redeindustrial.gestag.entidades.Config;
import com.redeindustrial.gestag.entidades.Usuario;
import com.redeindustrial.gestag.utils.Conexao;
import com.redeindustrial.gestag.utils.Contador;
import com.redeindustrial.gestag.utils.Persistencia;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private TextView tv_subdominio;
    private EditText et_email;
    private EditText et_senha;

    private View progress;

    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init_componentes();

        config = Config.getInstance(this);

        if(config.subdominio != null) {
            tv_subdominio.setText(config.subdominio);
        } else {
            alterarSubdominio(null);
        }


    }

    private void init_componentes() {
        tv_subdominio = findViewById(R.id.tv_subdominio);
        et_email = findViewById(R.id.login_email);
        et_senha = findViewById(R.id.login_senha);
        progress = findViewById(R.id.progress);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Contador.iniciarContagem();
    }

    // ações dos botões da tela

    public void abrirCadastro(View view) {
        startActivity(new Intent(this, CadastroActivity.class));
    }

    public void alterarSubdominio(View view) {

        EditText input = new EditText(LoginActivity.this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
        input.setHint(R.string.hint_subdominio);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(input)
                .setTitle(R.string.subdominio)
                .setPositiveButton(R.string.confirmar, null)
                .setNegativeButton(R.string.cancelar, (dialog1, which) -> {
                    if(config.subdominio == null) {
                        finish();
                        Contador.iniciarContagem();
                        Toast.makeText(LoginActivity.this, getString(R.string.insira_um_subdominio), Toast.LENGTH_LONG).show();
                    }
                }).create();

        if(config.subdominio != null) {
            input.setText(config.subdominio);
        } else {
            dialog.setCancelable(false);
        }

        float dpi = getResources().getDisplayMetrics().density;

        dialog.setView(input, (int) (20*dpi), (int) (5*dpi), (int) (20*dpi), (int) (5*dpi));
        dialog.show();

        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {

            config.subdominio = input.getText().toString().trim();

            if(config.subdominio.isEmpty()) {
                input.setError(getString(R.string.erro_campo_vazio));
                return;
            }

            if(Persistencia.salvar(this, config, Config.EXTRA)) {
                tv_subdominio.setText(config.subdominio);
                dialog.dismiss();
            }

        });

    }

    public void esqueciSenha(View view) {

        EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        input.setHint(R.string.hint_email);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage("Enviaremos um link para o seu e-mail para redefinir sua senha")
                .setPositiveButton(R.string.confirmar, null)
                .setNegativeButton(R.string.cancelar, null)
                .create();

        float dpi = getResources().getDisplayMetrics().density;

        dialog.setView(input, (int) (20*dpi), (int) (5*dpi), (int) (20*dpi), (int) (5*dpi));
        dialog.show();

    }

    public void entrar(View view) {

        boolean valido = true;

        String email = et_email.getText().toString().trim();
        String senha = et_senha.getText().toString();

        if(TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("Digite um e-mail válido");
            valido = false;
        }

        if(senha.length() < 6) {
            et_senha.setError("Digite uma senha de pelo menos 6 caracteres");
            valido = false;
        }

        if(valido) {

            Map<String, String> parametros = new HashMap<>();
            parametros.put("email", email);
            parametros.put("password", senha);

            Log.i("teste", "veio até aqui");

            new Conexao(progress) {

                @Override
                public void retorno(JSONObject json) throws JSONException {

                    if(json.getBoolean("success")) {

                        JSONObject data = json.getJSONObject("data");
                        config.usuario = new Usuario(data, genAuthKey(email, senha));

                        if(Persistencia.salvar(LoginActivity.this, config, Config.EXTRA)) {

                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        }

                    } else {
                        Toast.makeText(LoginActivity.this, json.getString("error_description"), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void erro(String erro) {
                    Toast.makeText(LoginActivity.this, erro, Toast.LENGTH_SHORT).show();
                }

            }.executeAsync("http://dev.gestag.com.br/api/v1/login", parametros, Conexao.Tipo.POST, null);

        }

    }

    public static String genAuthKey(String email, String senha) {
        String key = email + ":" + senha;
        byte[] encodedBytes = Base64.encode(key.getBytes(), Base64.NO_WRAP);
        return new String(encodedBytes, StandardCharsets.UTF_8);
    }

}