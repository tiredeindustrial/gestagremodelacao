package com.redeindustrial.gestag;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.redeindustrial.gestag.entidades.Config;
import com.redeindustrial.gestag.utils.Contador;
import com.redeindustrial.gestag.utils.Persistencia;

public class SplashActivity extends AppCompatActivity {

    private final int DURACAO = 4;

    private final Handler handler = new Handler();
    private final Runnable runnable = this::iniciarAplicacao;

    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init_configuracoes();

        if(Contador.trintaSeg) {
            iniciarAplicacao();
            return;
        }

        hideSystemUI();
        setContentView(R.layout.activity_splash);

        ImageView logo = findViewById(R.id.logo);
        logo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.scale));

        handler.postDelayed(runnable, DURACAO * 1000);

    }

    private void init_configuracoes() {

        config = Config.getInstance(this);

        switch (config.modo_noturno) {

            case Config.MODO_NOTURNO_ATIVADO:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;
            case Config.MODO_NOTURNO_DESATIVADO:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
            default:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                break;

        }

    }

    private void iniciarAplicacao() {

        Intent intent;

        if(config.usuario == null) {
            intent = new Intent(this, LoginActivity.class);
        } else {
            intent = new Intent(this, MainActivity.class);
        }

        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runnable);
        Contador.iniciarContagem();
    }

    public void toqueNaTela(View view) {
        iniciarAplicacao();
        handler.removeCallbacks(runnable);
    }

    private void hideSystemUI() {

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

    }

}