package com.redeindustrial.gestag.entidades;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Objeto implements Serializable {

    public static final String EXTRA = "com.redeindustrial.entidades.Objeto";

    private String codigo;
    private String descricao;
    private String mac;
    private int porta;
    private boolean status;

    public Objeto(JSONObject json) throws JSONException {
        this(json.getString("OBJ_CODIGO"), json.getString("OBJ_DESCRI"), json.getString("MAC"), json.getInt("PORTA"), json.getInt("STATUS_PADRAO") == 0);
    }

    public Objeto(String codigo, String descricao, String mac, int porta, boolean status) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.mac = mac;
        this.porta = porta;
        this.status = status;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getMac() {
        return mac;
    }

    public int getPorta() {
        return porta;
    }

    public boolean isStatus() {
        return status;
    }

}
