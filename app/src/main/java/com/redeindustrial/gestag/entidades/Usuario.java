package com.redeindustrial.gestag.entidades;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Base64;

public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    private String codigo;
    private String nome;
    private String email;
    private String telefone;
    private String permissao;
    private String token;

    public Usuario(JSONObject object, String token) throws JSONException {
        this(object.getString("USU_CODIGO"), object.getString("NOME_COMPLETO"), object.getString("EMAIL"), object.getString("TELEFONE"), object.getString("PERMISSAO"), token);
    }

    public Usuario(String codigo, String nome, String email, String telefone, String permissao, String token) {
        this.codigo = codigo;
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.permissao = permissao;
        this.token = token;
    }

    /* Getters e Setters */

    public String getCodigo() {
        return codigo;
    }

    public String getNome() { return nome; }

    public void setNome(String nome) { this.nome = nome; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getTelefone() { return telefone; }

    public void setTelefone(String telefone) { this.telefone = telefone; }

    public String getPermissao() {
        return permissao;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
