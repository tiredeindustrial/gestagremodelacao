package com.redeindustrial.gestag.entidades;

import android.content.Context;

import com.redeindustrial.gestag.utils.Persistencia;

import java.io.Serializable;

public class Config implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String EXTRA = "configs";
    public static final int MODO_NOTURNO_ATIVADO = 1, MODO_NOTURNO_DESATIVADO = 2;

    public String subdominio = null;
    public Usuario usuario = null;
    public int modo_noturno = MODO_NOTURNO_DESATIVADO;

    private Config() {}

    public static Config getInstance(Context context) {

        Config instance = Persistencia.carregar(context, Config.EXTRA);

        if(instance == null) {
            instance = new Config();
            Persistencia.salvar(context, instance, EXTRA);
        }

        return instance;

    }

}
