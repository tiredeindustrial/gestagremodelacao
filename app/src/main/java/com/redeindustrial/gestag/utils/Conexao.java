package com.redeindustrial.gestag.utils;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public abstract class Conexao {

    private final Executor executor = Executors.newSingleThreadExecutor(); // change according to your requirements
    private final Handler handler = new Handler(Looper.getMainLooper());

    private View progress;

    public enum Tipo {
        GET, POST
    }

    public Conexao() {}

    public Conexao(View progress) {
        this.progress = progress;
    }

    public void executeAsync(final String url, Map<String, String> parametros, Tipo tipo, String token) {

        if(progress != null)
            progress.setVisibility(View.VISIBLE);

        executor.execute(() -> {

            StringBuilder finalUrl = new StringBuilder(url);

            OkHttpClient cliente = new OkHttpClient();

            Request.Builder requestBuilder = new Request.Builder();

            if(token != null) {
                requestBuilder.addHeader("Authorization", "Basic "+token);
            }

            if(tipo == Tipo.POST) {

                MultipartBody.Builder body = new MultipartBody.Builder();
                body.setType(MultipartBody.FORM);

                if(parametros == null || parametros.size() == 0) {
                    body.addFormDataPart("", "");
                } else {
                    for (Map.Entry<String, String> entry : parametros.entrySet()) {
                        body.addFormDataPart(entry.getKey(), entry.getValue());
                    }
                }

                body.build();

                RequestBody requestBody = body.build();
                requestBuilder.post(requestBody);

            } else if(parametros != null && parametros.size() > 0) {

                finalUrl.append("?");
                for (Map.Entry<String, String> entry : parametros.entrySet()) {
                    finalUrl.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }

                finalUrl = new StringBuilder(finalUrl.substring(0, finalUrl.length() - 1));

            }

            requestBuilder.url(finalUrl.toString());

            Request request = requestBuilder.build();

            try {

                Response response = cliente.newCall(request).execute();
                Log.i("teste", "response");
                String string = response.body().string();

                Log.i("teste", "body.string");

                handler.post(() -> {

                    if(progress != null)
                        progress.setVisibility(View.GONE);

                    try {
                        retorno(new JSONObject(string));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        erro(string);
                    }

                });

            } catch (IOException e) {

                handler.post(() -> {

                    if(progress != null)
                        progress.setVisibility(View.GONE);

                    erro(e.getMessage());

                });

                //Log.i("teste", "Erro: "+e.toString());
                e.printStackTrace();

            }

        });

    }

    public abstract void retorno(JSONObject json) throws JSONException;
    public abstract void erro(String erro);

}
