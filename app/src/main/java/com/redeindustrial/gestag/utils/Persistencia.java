package com.redeindustrial.gestag.utils;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Persistencia {

    public static boolean salvar(Context context, Serializable objeto, String nome) {

        try {
            FileOutputStream fos = new FileOutputStream(context.getFileStreamPath(nome));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(objeto);
            oos.close();
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("teste", e.getLocalizedMessage());
            return false;
        }

    }

    public static <T extends Serializable> T carregar(Context context, String nome) {

        try {
            FileInputStream fis = new FileInputStream(context.getFileStreamPath(nome));
            ObjectInputStream ois = new ObjectInputStream(fis);
            T s = (T) ois.readObject();
            fis.close();
            ois.close();
            return s;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            Log.i("teste", e.getLocalizedMessage());
            return null;
        }

    }

}
