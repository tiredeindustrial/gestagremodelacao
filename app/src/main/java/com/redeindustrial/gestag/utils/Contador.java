package com.redeindustrial.gestag.utils;

import android.os.Handler;

public class Contador {

    private static final int SEGUNDOS = 15;

    public static boolean trintaSeg;
    public static Handler handler;
    public static Runnable runnable = new Runnable() {
        @Override
        public void run() {
            trintaSeg = false;
            handler = null;
        }
    };

    public static void iniciarContagem() {

        if(handler != null) {
            handler.removeCallbacks(runnable);
        } else {
            handler = new Handler();
        }
        trintaSeg = true;
        handler.postDelayed(runnable, SEGUNDOS * 1000);

    }

}
