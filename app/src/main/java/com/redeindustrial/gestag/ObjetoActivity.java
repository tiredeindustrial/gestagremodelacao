package com.redeindustrial.gestag;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.redeindustrial.gestag.entidades.Objeto;

public class ObjetoActivity extends AppCompatActivity {

    private Objeto objeto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objeto);

        objeto = (Objeto) getIntent().getSerializableExtra(Objeto.EXTRA);

        setTitle(objeto.getDescricao());

    }
}